let arr = [], ei,ej;
let event;
let timeGame;
let message = document.createElement('div');
let tableGame = document.querySelector('.tableGame');

//Ф-ия меняет два элемента массива с заданными индексами//
const swapElement = (arr,i1,j1,i2,j2) => {				
	let element = arr[i1][j1];
	arr[i1][j1] = arr[i2][j2];
	arr[i2][j2] = element;
}

window.addEventListener('load', function() {				
	newGame();				
	document.querySelector('.reset').addEventListener('click', newGame);		
});

const cellClick = (event) => {
	let target = event.target;
	let i = target.id.charAt(0);
	let j = target.id.charAt(2);

	event = event || window.event;

//Если пустая ячейка расположена рядом с той по которой кликнули на расстоянии 1 клетки, то меняем их местами//
	if((i == ei && Math.abs(j - ej) == 1) || (j == ej && Math.abs(i - ei) == 1)){					
		document.getElementById(ei + " " + ej).innerHTML = target.innerHTML;
		target.innerHTML = "";
		ei = i;
		ej = j;
		let flag = true;
		for(let i = 0; i < 4; ++i) {
			for(let j = 0; j < 4; ++j) {
//Если пустая клетка справа снизу//
				if(i + j != 6 && document.getElementById(i + " " + j).innerHTML != i*4 + j + 1){
					flag = false;
					break;
				}
			}
		}
		if(flag) {
//Получаем текущее время//
			DataFinish = new Date();
			MinutesFinish = DataFinish.getMinutes();
			SecondsFinish = DataFinish.getSeconds();
			timeGame = `${(MinutesFinish - MinutesStart)} : ${(+(SecondsFinish - SecondsStart))}`;
		
			message = document.createElement('div');
			message.classList.add('message');

			setTimeout(() => {
				message.innerHTML = `Ура! Вы решили головоломку за ${timeGame} !`;
				tableGame.append(message);
			},100);
		} 
	}
}

const newGame =() => {			

//Получаем начальное время//
	DataStart = new Date();
	MinutesStart = DataStart.getMinutes();
	SecondsStart = DataStart.getSeconds();
	
//Проверяем не в выигрышной ли комбинации находятся элементы//
	for(let i = 0; i < 4; ++i){
		arr[i] = [];

		for(let j = 0; j < 4; ++j){
			if(i + j != 6) {
				arr[i][j] = i*4 + j + 1;
			} else {
				arr[i][j] = "";
			}
		}
	}
	ei = 3;
	ej = 3;

//Перемешиваем элементы при загрузке игры//
	for(let i = 0; i < 1600; ++i) {

		switch(Math.round(3*Math.random())){
			case 0: if(ei != 0) swapElement(arr,ei,ej,--ei,ej); break; // вверх
			case 1: if(ej != 3) swapElement(arr,ei,ej,ei, ++ej); break; // напрво
			case 2: if(ei != 3) swapElement(arr,ei,ej,++ei,ej); break; // вниз
			case 3: if(ej != 0) swapElement(arr,ei,ej,ei,--ej); // влево
		}
	}

	let table = document.createElement('table');
	let tbody = document.createElement('tbody');					
	table.append(tbody);

//Рисуем строки и ячейки//
	for(let i = 0; i < 4; ++i){
		let row = document.createElement('tr');

		for(let j = 0; j < 4; ++j){
			let cell = document.createElement('td');
			cell.id = i + " " + j;
			cell.addEventListener('click', cellClick);
			cell.innerHTML = arr[i][j];
			row.append(cell);
		}
		tbody.append(row);					
	}

	if(tableGame.childNodes.length >= 1) {
		while (tableGame.firstChild) {
			tableGame.removeChild(tableGame.firstChild);
		}
	}
	tableGame.appendChild(table);	
}